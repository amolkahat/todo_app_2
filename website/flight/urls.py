from django.urls import path

from .views import index, booking, about

urlpatterns = [
    path('', index, name='index'),
    path('about/', about, name='about'),
    path('booking/', booking, name='booking'),
    path('booking/<str:f_id>', booking, name='booking')
]
